package com.deft.swagger.entity;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@ToString
@Entity
@Table(name = "board",indexes = {})
@NamedQuery(name = "Board.findAll", query = "SELECT c FROM Board c")
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="title" , columnDefinition="VARCHAR(64)")
    @NotNull
    private String title;

    @Column(name="content" , columnDefinition="VARCHAR(64)")
    @NotNull
    private String content;

    @Column(name="author" , columnDefinition="VARCHAR(64)")
    private String author;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time")
    private Date createTime;

    public Board() {

    }
}
