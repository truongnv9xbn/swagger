package com.deft.swagger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.util.Objects;

@Entity(name = "sys_cfg")
@Table(name = "sys_cfg")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Cfg extends BaseSysEntity {

//    @NotBlank(message = "name cannot be empty")empty
    @Column(name = "cfg_name", columnDefinition = "VARCHAR(256) default '' ")
    private String cfgName = "";

//    @NotBlank(message = "value cannot be empty")
    @Column(name = "cfg_value", columnDefinition = "VARCHAR(512) default ''")
    private String cfgValue = "";

    @Column(name = "cfg_desc", columnDefinition = "TEXT")
    private String cfgDesc;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Cfg cfg = (Cfg) o;
        return getId() != null && Objects.equals(getId(), cfg.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
