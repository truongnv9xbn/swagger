package com.deft.swagger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.util.Objects;

@Entity(name = "sys_notice")
@Table(name = "sys_notice")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Notice extends BaseSysEntity {
    @Column
    private String title;
    @Column
    private Integer type;
    @Column
    private String content;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Notice notice = (Notice) o;
        return getId() != null && Objects.equals(getId(), notice.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
