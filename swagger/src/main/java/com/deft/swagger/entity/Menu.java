package com.deft.swagger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity(name = "sys_menu")
@Table(name = "sys_menu")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Menu extends BaseSysEntity {

    @Column(columnDefinition = "VARCHAR(32)", unique = true, nullable = false)
    @NotBlank(message = "code cannot be empty")
    @Valid
    private String code;

    @Column(columnDefinition = "VARCHAR(64)", nullable = false)
    private String pcode;

    @Column(columnDefinition = "VARCHAR(128)")
    private String pcodes;

    @Column(columnDefinition = "VARCHAR(64)", nullable = false)
    @NotBlank(message = "name cannot be empty")
    @Valid
    private String name;

    @Column(columnDefinition = "VARCHAR(32)")
    private String icon;

    @Column(columnDefinition = "VARCHAR(32)")
    private String url;

    @Column(columnDefinition = "INT", nullable = false)
    private Integer num;

    @Column(columnDefinition = "INT", nullable = false)
    private Integer levels;

    @Column(columnDefinition = "INT", nullable = false)
    private Integer ismenu;

    @Column(columnDefinition = "VARCHAR(32)")
    private String tips;

    @Column(columnDefinition = "INT")
    private Integer isopen;

    @Column(columnDefinition = "VARCHAR(128)")
    private String component;

    @Column
    private Short hidden = 0;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Menu menu = (Menu) o;
        return getId() != null && Objects.equals(getId(), menu.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
