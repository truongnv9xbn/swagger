package com.deft.swagger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;


@Entity(name = "sys_relation")
@Table(name = "sys_relation")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Relation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long menuid;
    @Column
    private Long roleid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Relation relation = (Relation) o;
        return id != null && Objects.equals(id, relation.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
