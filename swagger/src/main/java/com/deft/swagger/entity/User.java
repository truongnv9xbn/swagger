package com.deft.swagger.entity;

import com.deft.swagger.dto.UserVO;
import lombok.Data;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "u_user", indexes = {
})
@Data
@ToString
@NamedQuery(name = "User.findAll", query = "select c from User c")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    @SequenceGenerator(name="user_generator", sequenceName = "user_seq", initialValue = 1000, allocationSize=1)
    private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time")
    private Date createTime;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_time")
    private Date modifyTime;

    @NotNull
    @Column(name = "part",columnDefinition="SMALLINT")
    private Short part=0;

    @NotNull
    @Column(name = "app_user_id",columnDefinition="bigint")
    private Long appUserId;

    @Column(name = "phone",columnDefinition="VARCHAR(25)")
    private String phone;

    @Column(name = "full_name",columnDefinition="VARCHAR(100)")
    private String fullName;

    @Column(name = "email",columnDefinition="VARCHAR(25)")
    private String email;

    @Column(name = "phone_country_code",columnDefinition="VARCHAR(10)")
    private String phoneCountryCode;

    @NotNull
    @Column(name = "is_locked",columnDefinition="SMALLINT")
    private Integer isLocked = 0;

    @Column(name = "locked_reason",columnDefinition="VARCHAR(10)")
    private String lockedReason;

    @NotNull
    @Column(name = "is_deleted",columnDefinition="SMALLINT")
    private Integer isDeleted = 0;

    @Column(name = "otp_blocked",columnDefinition="SMALLINT")
    private Integer otpBlocked = 0;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date otpBlockedDate;

    @Column(name="status" , columnDefinition="SMALLINT")
    @NotNull
    private Integer status = 1;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date deletedDate;

    @Column(name = "contract_url", columnDefinition="VARCHAR(256)")
    private String contractUrl;

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public User(UserVO userVO) {
        BeanUtils.copyProperties(userVO, this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return id != null && Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
