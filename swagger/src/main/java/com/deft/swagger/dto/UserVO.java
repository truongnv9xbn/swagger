package com.deft.swagger.dto;

import com.deft.swagger.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserVO extends BaseVO {
    private Long id;

    private Long appUserId;

    private String phone;

    private String phoneCountryCode;

    private String fullName;

    private String email;

    private Integer status;

    public UserVO() {
    }
    public UserVO(Long id) {
        this.id = id;
    }

    public UserVO(User user) {
        if(user != null){
            BeanUtils.copyProperties(user, this);
        }
    }
}
