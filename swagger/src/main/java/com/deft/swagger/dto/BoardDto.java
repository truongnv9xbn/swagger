package com.deft.swagger.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
public class BoardDto {
    private Long id;
    private String title;
    private String content;
    private String author;
    private Date createTime;
}
